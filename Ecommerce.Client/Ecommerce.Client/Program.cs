﻿using Ecommerce.Core;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Ecommerce.Client
{
    class Program
    {
        static void Main(string[] args)
        {
            string password = Console.ReadLine();

            try
            {
                var arrayIndirizzi = new Indirizzo[]
                {
                    new Indirizzo("via Verdi 35", "Macerata", "62100"),
                    new Indirizzo("via Verdi 1", "Camerino", "62032")
                };

                var x = new List<object>();

                var indirizzi = new List<Indirizzo>()
                {
                    new Indirizzo("via Verdi 35", "Macerata", "62100"),
                    new Indirizzo("via Verdi 1", "Camerino", "62032")
                };

                //indirizzi.Add(new Indirizzo("via Verdi 35", "Macerata", "62100"));
                //indirizzi.Add(new Indirizzo("via Verdi 1", "Camerino", "62032"));

                var c = new Cliente("User1", password, "Mario", "Rossi",
                    "alexglabs@gmail.com", DateTime.Now, "ABC123.....", arrayIndirizzi);


                Console.WriteLine("Cliente creato correttamente.");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            //c.ModificaAnagrafica("Luca", "Esposito", "CF1234567");

        }
    }
}
