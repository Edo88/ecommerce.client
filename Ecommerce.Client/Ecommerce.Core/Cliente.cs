﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ecommerce.Core
{
    public class Cliente
    {
        private string username;
        public string Username
        {
            get { return username; }
            private set { username = value; }
        }

        public string Password { get; private set; }

        public string Nome { get; private set; }
        public string Cognome { get; private set; }
        public string Email { get; private set; }
        public string CodiceFiscale { get; private set; }

        public DateTime DataRegistrazione { get; private set; }

        public IEnumerable<Indirizzo> Indirizzi { get; private set; }


        public Cliente(string username, string password, string nome,
            string cognome, string email, DateTime dataRegistrazione,
            string codiceFiscale, IEnumerable<Indirizzo> indirizzi)
        {
            if (password.Length < 8)
                throw new ArgumentException("La password dev'essere lunga almeno 8 caratteri.");

            if (!Utils.IsEmailValida(email))
                throw new ArgumentException("Email non valida.");

            this.Username = username;
            this.Password = password;
            this.Nome = nome;
            this.Cognome = cognome;
            this.Email = email;
            this.DataRegistrazione = dataRegistrazione;
            this.CodiceFiscale = codiceFiscale;
            this.Indirizzi = indirizzi;
        }

        public void ModificaEmail(string email)
        {
            if (!Utils.IsEmailValida(email))
                throw new ArgumentException("Email non valida.");

            this.Email = email;
        }

        public void ModificaAnagrafica(string nome, string cognome,
            string codiceFiscale)
        {

            this.Nome = nome;
            this.Cognome = cognome;
            this.CodiceFiscale = codiceFiscale;
        }

        public string Descrizione
        {
            get
            {
                return Nome + " " + Cognome + " (" + Email + ")";
            }
        }
    }
}
